#!/usr/bin/env bash

test -e /etc/provisioning.done && exit 0

yum install -y wget
wget http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
yum install -y epel-release-6-8.noarch.rpm
yum install -y gcc gdb ccache make bison flex git diffutils
yum install -y zlib-devel readline-devel gettext \
    opensp openjade \
    docbook-style-dsssl docbook-style-xsl docbook-dtds docbook-utils \
    libxml2-devel libxslt-devel openssl-devel openldap-devel \
    tcl-devel python-devel perl-devel perl-ExtUtils-Embed

mkdir -p bf/root bf/ccache
cd bf
wget -O buildfarm.tgz http://www.pgbuildfarm.org/downloads/latest-client.tgz 
tar -z --strip-components=1 -xf buildfarm.tgz

sed -i\
 -e 's!/path/to/buildroot!/home/vagrant/bf/root!' \
 -e 's!scmrepo => undef!scmrepo => "https://github.com/postgres/postgres.git"!' \
 -e 's!CCACHE_DIR.*!CCACHE_DIR => "/home/vagrant/bf/ccache/$branch",!' \
 -e '/scm_url =>/a\
     git_keep_mirror => "true",\
     ignore_mirror_failure => "true",' build-farm.conf

chown -R vagrant:vagrant .

echo After login, do: 
echo "    cd ~vagrant/bf && perl run_build.pl --test --verbose"
echo " "
echo The very first checkout will take a while as the git repository is fetched.

touch /etc/provisioning.done
